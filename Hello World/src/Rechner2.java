import java.util.Scanner;

public class Rechner2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int zahl1 = 0, zahl2 = 0, adderg = 0, diverg = 0;
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Bitte geben Sie eine ganze Zahl ein: ");
		zahl1 = sc.nextInt();
		
		System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");
		zahl2 = sc.nextInt();
		
		adderg = zahl1 + zahl2;
		diverg = zahl1 / zahl2;
		
		System.out.print("\n\n\nErgebnis der Addition lautet: ");
		System.out.print(zahl1 + " + " + zahl2 + " = " + adderg);
		
		System.out.print("\nErgebnis der Subtraktion lautet: ");
		System.out.print(zahl1 + " - " + zahl2 + " = " + (zahl1 - zahl2)  );  
		
		System.out.print("\nErgebnis der Division lautet: ");
	    System.out.print(zahl1 + " : " + zahl2 + " = " + diverg );
	    
	    System.out.print("\nErgebnis der Multiplikation lautet: ");
	    System.out.print(zahl1 + " * " + zahl2 + " = " + (zahl1 * zahl2) );        
	    
	    sc.close();
		
	}

}
