
public class Operatoren {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int zahl1;
		int zahl2;
		
		System.out.println("UEBUNG ZU OPERATOREN IN JAVA");
		System.out.println();
		
		zahl1 = 75;
		zahl2 = 23;
		System.out.println("Ganzzahl a ist gleich "+zahl1+" und Ganzzahl b ist "+zahl2);
		
		int summe = zahl1+zahl2;
		System.out.println("a + b= "+ (zahl1+zahl2));
		System.out.println();
		
		System.out.println("a % b = "+(zahl1%zahl2));
		System.out.println("a / b = "+(zahl1/zahl2));
		System.out.println("a * b = "+(zahl1*zahl2));
		System.out.println("a - b = "+(zahl1-zahl2));
		System.out.println();
		
		System.out.println("Ist a = b?  " +(zahl1==zahl2));
		System.out.println();
		
		System.out.println("Ist a <= b? "+(zahl1<=zahl2));
		System.out.println("Ist a >= b? "+(zahl1>=zahl2));
		System.out.println("Ist a < b? "+(zahl1<zahl2));
		System.out.println("Ist a > b? "+(zahl1>zahl2));
		System.out.println();
		
		boolean aInIntervall = (zahl1>=0) && (zahl2<=50);
		boolean bInIntervall = (zahl2>=0) && (zahl1<=50);
		System.out.println("Ist a in [0;50]? "+aInIntervall);
		System.out.println("Ist b in [0;50]? "+bInIntervall);
		System.out.println();
	}

}
