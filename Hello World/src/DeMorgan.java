
public class DeMorgan {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

        boolean a = false, b = false;
        System.out.printf("                    _______    _   _\n");
        System.out.printf("|   a    |   b    | (a ^ b) |  a v b  |\n");
        System.out.printf("---------------------------------------\n");
        System.out.printf("| %-6b | %-6b | %-7b | %-7b |\n", a, b, !( a && b ), (!a || !b) );
        a = false; b = true;
        System.out.printf("| %-6b | %-6b | %-7b | %-7b |\n", a, b, !( a && b ), (!a || !b) );
        a = true; b = false;
        System.out.printf("| %-6b | %-6b | %-7b | %-7b |\n", a, b, !( a && b ), (!a || !b) );
        a = true; b = true;
        System.out.printf("| %-6b | %-6b | %-7b | %-7b |\n", a, b, !( a && b ), (!a || !b) );

        System.out.println("\n");

        a = false; b = false;
        System.out.printf("                    _______    _   _\n");
        System.out.printf("|   a    |   b    | (a v b) |  a ^ b  |\n");
        System.out.printf("---------------------------------------\n");
        System.out.printf("| %-6b | %-6b | %-7b | %-7b |\n", a, b, !( a || b ), (!a && !b) );
        a = false; b = true;
        System.out.printf("| %-6b | %-6b | %-7b | %-7b |\n", a, b, !( a || b ), (!a && !b) );
        a = true; b = false;
        System.out.printf("| %-6b | %-6b | %-7b | %-7b |\n", a, b, !( a || b ), (!a && !b) );
        a = true; b = true;
        System.out.printf("| %-6b | %-6b | %-7b | %-7b |\n", a, b, !( a || b ), (!a && !b) );

	}

}
