package konsolenausgabe;

public class konsolenausgabeeinspunktfünf {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Aufgabe 1");
         System.out.println("Das ist ein \"Beispielsatz\".");
         System.out.println("Ein Beispielsatz ist das.");
         // bei "print" wird nur der Satz innerhalb der Klammern ausgegeben. Bei "println" wird zusätzlich noch ein Zeilenvorschub gemacht.
         // "Ich heiße Jean-Paul und bin 31 Jahre alt." ausgeben
         int alter = 31;
         String name = "Jean-Paul";
         System.out.println("Ich heiße " + name + " und bin " + alter + " Jahre alt.");
         System.out.println("");
         System.out.println("Aufgabe 2");
         String s = "*";
         System.out.printf( "%6s\n", s );  
         System.out.printf( "%7s\n", "***" ); 
         System.out.printf( "%8s\n", "*****" );
         System.out.printf( "%9s\n", "*******" ); 
         System.out.printf( "%10s\n", "*********" );
         System.out.printf( "%11s\n", "***********" ); 
         System.out.printf( "%7s\n", "***" ); 
         System.out.printf( "%7s\n", "***" ); 
         System.out.printf( "%7s\n", "***" ); 
         System.out.println("");
         System.out.println("Aufgabe 3");
         
         double d = 22.4234234;
         double e = 111.2222;
         double f = 4.0;
         double g = 1000000.551;
         double h = 97.34;

         System.out.printf( "%.2f\n" , d);
         System.out.printf( "%.2f\n" , e);
         System.out.printf( "%.2f\n" , f);
         System.out.printf( "%.2f\n" , g);
         System.out.printf( "%.2f\n" , h);
	}

}
