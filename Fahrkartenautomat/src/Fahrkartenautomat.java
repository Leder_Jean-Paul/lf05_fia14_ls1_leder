﻿import java.util.Scanner;

class Fahrkartenautomat
{
	public static void main(String[] args)
	{        
		double eingezahlterGesamtbetrag;       
		double rückgabebetrag;   
		while (true) { //Aufgabe 4.5_2
		double ticketPreisGesamt = fahrkartenbestellungErfassen();

		// Geldeinwurf
		// -----------
		eingezahlterGesamtbetrag = fahrkartenBezahlen(ticketPreisGesamt);

		// Fahrscheinausgabe
		// -----------------
		fahrkartenAusgeben();

		// Rückgeldberechnung und -Ausgabe
		// -------------------------------
		rückgabebetrag = eingezahlterGesamtbetrag - ticketPreisGesamt; // 2 Operationen: 1. Differenz, 2. Zuweisung
		rueckgeldAusgeben(rückgabebetrag);
		}
	}



	private static double fahrkartenbestellungErfassen() {
		Scanner tastatur = new Scanner(System.in);

		double gesamtPreis = 0;
		boolean auswahlFertig = false;
		
		String[] bezeichnung = {"Einzelfahrschein Berlin AB", 
				"Einzelfahrschein Berlin BC", "Einzelfahrschein Berlin ABC", 
				"Kurzstrecke", "Tageskarte Berlin AB", 
				"Tageskarte Berlin BC", "Tageskarte Berlin ABC", 
				"Kleingruppen-Tageskarte Berlin AB","Kleingruppen-Tageskarte Berlin BC", 
				"Kleingruppen-Tageskarte Berlin ABC"};
		
		double[] preisInEuro = {2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90, 0};
		
		while(true) {
			double ticketpreisEingabe = 0;
			System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:\n");
			for (int z = 0;z < bezeichnung.length; z++) {
				System.out.printf("(" + (z) + ") " + bezeichnung[z] + " (%.2f EURO)\n", preisInEuro[z]);
			}
			System.out.println("(" + (bezeichnung.length) + ") Bezahlen");

			boolean validInput = false;
			while(validInput == false) {
				System.out.print("Ihre Wahl: ");
				int auswahl = tastatur.nextInt();
				
				if(auswahl < 0 || auswahl > bezeichnung.length) {
					System.out.println(">> falsche Eingabe <<");
				} else {
					if(auswahl == bezeichnung.length) {
						auswahlFertig = true;
						break;
					}
					//Aufgabe 5.3: Vorteile: - einfachere Bearbeitung
										   //- restliche Implementierung muss nicht angefasst werden, wenn Werte verändert werden sollen (z.B. Preiserhöhungen).
								 //Nachteile: - initiale Implementierung komplizierter und aufwendiger
											//- Übersichtlichkeit kann darunter leiden
					ticketpreisEingabe = preisInEuro[auswahl];
					validInput = true;
				}				
			}

			if(auswahlFertig)
				break;		

			while(true) {
				System.out.print("Anzahl der Tickets: ");

				int anzahlDerTickets = tastatur.nextInt();

				if (anzahlDerTickets < 1 || anzahlDerTickets > 10) {

					System.out.println(">> Wählen Sie bitte eine Anzahl von 1 bis 10 Tickets aus. <<");
				}
				else {
					gesamtPreis += ticketpreisEingabe * anzahlDerTickets;
					break;					
				}
			}
			
			System.out.printf("Zwischensumme: %.2f €\n", gesamtPreis);
		}
		
		return gesamtPreis;
	}

	private static double fahrkartenBezahlen(double zuZahlen) {
		Scanner tastatur = new Scanner(System.in);

		double eingezahlterGesamtbetrag = 0.0; 
		while(eingezahlterGesamtbetrag < zuZahlen) 
		{
			System.out.printf("Noch zu zahlen: %.2f Euro\n", zuZahlen - eingezahlterGesamtbetrag);
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");     	   
			
			String eingeworfeneMuenze = tastatur.next();
			eingeworfeneMuenze = eingeworfeneMuenze.replace(',', '.');
			int geldAlsCent = (int)(Double.valueOf(eingeworfeneMuenze) * 100);			
			switch(geldAlsCent) {
			case 5:
			case 10:
			case 20:
			case 50:
			case 100:
			case 200:
				eingezahlterGesamtbetrag += (double)geldAlsCent / 100;
				break;
				default:
					System.out.println(">> Die eingeworfene Münze ist nicht gültig. (" + eingeworfeneMuenze + ")");
					break;
			}
		}

		return eingezahlterGesamtbetrag;
	}

	private static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++)
		{
			System.out.print("=");
			warte (250);
		}
		System.out.println("\n\n");
	}

	private static void warte(int millisekunden)
	{   
		try {
			Thread.sleep(millisekunden);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	private static void rueckgeldAusgeben(double rückgabebetrag) {
		if(rückgabebetrag > 0.0) 
		{
			System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO\n", rückgabebetrag);
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while(rückgabebetrag >= 2.0) 
			{
				muenzeAusgeben(2, "EURO");
				rückgabebetrag -= 2.0; 
			}
			while(rückgabebetrag >= 1.0) 
			{
				muenzeAusgeben(1, "EURO");
				rückgabebetrag -= 1.0; 
			}
			while(rückgabebetrag >= 0.5) 
			{
				muenzeAusgeben(50, "CENT");				
				rückgabebetrag -= 0.5; 
			}
			while(rückgabebetrag >= 0.2) 
			{
				muenzeAusgeben(20, "CENT");
				rückgabebetrag -= 0.2; 
			}
			while(rückgabebetrag >= 0.1) 
			{
				muenzeAusgeben(10, "CENT");
				rückgabebetrag -= 0.1; 
			}
			while(rückgabebetrag >= 0.05)
			{
				muenzeAusgeben(5, "CENT");
				rückgabebetrag -= 0.05; 
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
				"vor Fahrtantritt entwerten zu lassen!\n"+
				"Wir wünschen Ihnen eine gute Fahrt.\n");
	}
	
	private static void muenzeAusgeben(int betrag, String einheit) {
		String betragAlsString = Integer.toString(betrag);
			
		
		System.out.println("   * * *   ");
		System.out.println(" *       * ");
		
		// Betrag
		System.out.print("*    "+betragAlsString);
		for(int i= 0; i < 5 - betragAlsString.length(); i++) {
			System.out.print(" ");
		}
		System.out.println("*");
		
		// Währung		
		System.out.print("*   "+einheit);
		for(int i= 0; i < 6 - einheit.length(); i++) {
			System.out.print(" ");
		}
		System.out.println("*");
		
		System.out.println(" *       * ");
		System.out.println("   * * *   ");
	}
} 