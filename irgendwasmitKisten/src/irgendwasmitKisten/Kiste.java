package irgendwasmitKisten;

public class Kiste {

	private double hoehe;
	private double breite;
	private double tiefe;
	private String farbe;
	private double volume;
	
	public Kiste (double hoehe, double breite, double tiefe, String farbe) {
		this.hoehe = hoehe;
		this.breite = breite;
		this.tiefe = tiefe;
		this.farbe = farbe;
		this.volume = hoehe*breite*tiefe;
		}
	public double getVolume() {
		return volume;
	}
	public String getFarbe() {
		return farbe;
	}
	
public static void main(String[] args) {
	Kiste blau = new Kiste(10, 15, 16, "Blau");
	Kiste rot = new Kiste(8, 6, 10, "Rot");
	Kiste gelb = new Kiste(12, 14, 8, "Gelb");
	
	System.out.println(blau.getFarbe() + " " + blau.getVolume());
	System.out.println(rot.getFarbe() + " " + rot.getVolume());
	System.out.println(gelb.getFarbe() + " " + gelb.getVolume());
}	

}

