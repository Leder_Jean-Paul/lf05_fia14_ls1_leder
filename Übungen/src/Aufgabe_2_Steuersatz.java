import java.util.Scanner;

public class Aufgabe_2_Steuersatz {

	public static void main(String[] args) {

		double nettowert, bruttowert;
		String wahl;
		boolean t = false;
		final double steuersatz = 0.19;
		final double steuersatzErm��igt = 0.07;

		Scanner tastatur = new Scanner(System.in);
		System.out.println("Geben Sie ihren Nettowert an:");
		nettowert = tastatur.nextDouble();
		while (t != true) {
			System.out.println("Wird der erm��igte Steuersatz verwendet? [ja/nein]");
			wahl = tastatur.next();
			switch (wahl) {

			case "nein":
				bruttowert = nettowert / (1 - steuersatz);
				System.out.printf("%s%.2f%s", "Der Bruttowert betr�gt: " , bruttowert , "�");
				t = true;
				break;
			case "ja":
				bruttowert = nettowert / (1 - steuersatzErm��igt);
				System.out.printf("%s%.2f%s", "Der Bruttowert betr�gt: " , bruttowert , "�");
				t = true;
				break;
			default:
				System.out.println("Falsche Eingabe!");
				break;
			}
		}
	}

}
